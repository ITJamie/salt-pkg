import argparse
import hashlib
import os


def generate_hash_files(artifact_dir):
    hashes = ["blake2b", "sha512", "sha3_512"]
    data = {}
    for hash_type in hashes:
        data[hash_type] = {}
        # get the hashes.
        for root, dirs, files in os.walk(artifact_dir):
            for artifact in files:
                if not artifact.endswith(("tar.gz", "zip", "exe", "pkg", "msi")):
                    continue
                artifact_path = os.path.join(root, artifact)
                data[hash_type][artifact] = {}
                with open(artifact_path, "rb") as rfh:
                    raw = rfh.read()
                    hash_data = getattr(hashlib, hash_type)(raw).hexdigest()
                    if artifact.endswith(("tar.gz", "zip", "exe", "pkg", "msi")):
                        salt_version = artifact.rsplit("-", 3)[0]
                        pkg_version = artifact.rsplit("-", 3)[1]
                    data[hash_type][artifact]["hash"] = hash_data
                    data[hash_type][artifact]["pkg_name"] = salt_version
                    data[hash_type][artifact]["pkg_version"] = pkg_version

        # write the hashes to a file.
        for entry in data[hash_type].items():
            artifact_name = entry[0]
            artifact_data = entry[1]
            with open(
                os.path.join(
                    artifact_dir,
                    root.split("/")[-1],
                    artifact_data["pkg_name"]
                    + f"-{artifact_data['pkg_version']}"
                    + f"_{hash_type.upper()}",
                ),
                "a+",
            ) as fp:
                fp.write(f'{artifact_data["hash"]}  {artifact_name}\n')
    return data


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-a", "--artifact-dir", help="Path where the artifacts are located"
    )
    args = parser.parse_args()
    generate_hash_files(artifact_dir=args.artifact_dir)
