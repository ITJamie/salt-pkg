import sys

import pytest
from pytestskipmarkers.utils import platform


def test_salt_version(version, install_salt):
    """
    Test version outputed from salt --version
    """
    ret = install_salt.proc.run(*install_salt.binary_paths["salt"], "--version")
    assert ret.stdout.strip() == f"salt {version}"


def test_salt_versions_report_master(install_salt):
    """
    Test running --versions-report on master
    """
    ret = install_salt.proc.run(
        *install_salt.binary_paths["master"], "--versions-report"
    )
    ret.stdout.matcher.fnmatch_lines(["*Salt Version:*"])
    if sys.platform == "win32":
        ret.stdout.matcher.fnmatch_lines(["*Python: 3.8.13*"])
    elif sys.platform == "darwin":
        ret.stdout.matcher.fnmatch_lines(["*Python: 3.9.13*"])
    else:
        ret.stdout.matcher.fnmatch_lines(["*Python: 3.9.13*"])


def test_salt_versions_report_minion(salt_cli, salt_minion):
    """
    Test running test.versions_report on minion
    """
    ret = salt_cli.run("test.versions_report", minion_tgt=salt_minion.id)
    ret.stdout.matcher.fnmatch_lines(["*Salt Version:*"])


@pytest.mark.parametrize(
    "binary", ["master", "cloud", "syndic", "minion", "call", "api"]
)
def test_compare_versions(version, binary, install_salt):
    """
    Test compare versions
    """
    if platform.is_windows() and install_salt.singlebin:
        pytest.skip(
            "Already tested in `test_salt_version`. No need to repeat "
            "for windows single binary installs."
        )
    if binary in ["master", "cloud", "syndic"]:
        if sys.platform.startswith("win"):
            pytest.skip(f"{binary} not installed on windows")

    ret = install_salt.proc.run(*install_salt.binary_paths[binary], "--version")
    ret.stdout.matcher.fnmatch_lines([f"*{version}*"])


def test_compare_pkg_versions_redhat_rc(version, install_salt):
    """
    Test compare pkg versions for redhat RC packages.
    A tilde should be included in RC Packages and it
    should test to be a lower version than a non RC package
    of the same version. For example, v3004~rc1 should be
    less than v3004.
    """
    if install_salt.distro_id not in ("centos", "redhat", "amzn", "fedora"):
        pytest.skip("Only tests rpm packages")

    pkg = [x for x in install_salt.pkgs if "rpm" in x]
    if not pkg:
        pytest.skip("Not testing rpm packages")
    pkg = pkg[0].split("/")[-1]
    if "rc" not in pkg:
        pytest.skip("Not testing an RC package")
    assert "~" in pkg
    comp_pkg = pkg.split("~")[0]
    ret = install_salt.proc.run("rpmdev-vercmp", pkg, comp_pkg)
    ret.stdout.matcher.fnmatch_lines([f"{pkg} < {comp_pkg}"])
