import subprocess

import pytest


@pytest.mark.skip_on_windows(
    reason="Salt Master scripts not included in old windows packages"
)
def test_salt_upgrade(salt_call_cli, salt_minion, install_salt):
    """
    Test upgrade of Salt
    """
    if not install_salt.upgrade:
        pytest.skip("Not testing an upgrade, do not run")
    # verify previous install version is setup correctly and works
    ret = salt_call_cli.run("test.ping")
    assert ret.returncode == 0
    assert ret.data
    # upgrade Salt from previous version and test
    install_salt.install(upgrade=True)
    ret = salt_call_cli.run("test.ping")
    assert ret.returncode == 0
    assert ret.data


# this is only a workaround until we can test upgrades from
# previous tiamat packages instead.
# The Salt Master scripts are not included in the old packages
# So this is only testing minimal minion functionality for windows
# When tiamat packages are in production we will need to remove these
# two tests for windows.
@pytest.mark.skip_unless_on_windows()
def test_salt_upgrade_windows_1(install_salt):
    """
    Test upgrade of Salt on windows
    """
    if not install_salt.upgrade:
        pytest.skip("Not testing an upgrade, do not run")
    # verify previous install version is setup correctly and works
    ret = subprocess.run(["C:\\salt\\salt-call.bat", "--local", "test.ping"])
    assert ret.returncode == 0


@pytest.mark.skip_unless_on_windows()
def test_salt_upgrade_windows_2(salt_call_cli, salt_minion, install_salt):
    """
    Test upgrade of Salt on windows
    """
    if install_salt.no_uninstall:
        pytest.skip("Not testing an upgrade, do not run")
    # upgrade Salt from previous version and test
    install_salt.install(upgrade=True)
    ret = salt_call_cli.run("test.ping")
    assert ret.returncode == 0
    assert ret.data
