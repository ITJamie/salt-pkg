import datetime
import os
import pathlib
import shutil
import sys

import nox
from nox.command import CommandFailed

# Nox options
#  Reuse existing virtualenvs
nox.options.reuse_existing_virtualenvs = True
#  Don't fail on missing interpreters
nox.options.error_on_missing_interpreters = False

# Be verbose when runing under a CI context
# Be verbose when running under a CI context
CI_RUN = (
    os.environ.get("JENKINS_URL")
    or os.environ.get("CI")
    or os.environ.get("DRONE")
    or os.environ.get("GITHUB_ACTIONS")
) is not None
PIP_INSTALL_SILENT = CI_RUN is False
SKIP_REQUIREMENTS_INSTALL = os.environ.get("SKIP_REQUIREMENTS_INSTALL", "0") == "1"
EXTRA_REQUIREMENTS_INSTALL = os.environ.get("EXTRA_REQUIREMENTS_INSTALL")

COVERAGE_VERSION_REQUIREMENT = "coverage"

# Prevent Python from writing bytecode
os.environ["PYTHONDONTWRITEBYTECODE"] = "1"

# Global Path Definitions
REPO_ROOT = pathlib.Path(__file__).resolve().parent
# Change current directory to REPO_ROOT
os.chdir(REPO_ROOT)

# Coverage site customize
SITECUSTOMIZE_DIR = str(REPO_ROOT / "tests" / "support" / "coverage")
# Artifacts path
ARTIFACTS_DIR = REPO_ROOT / "artifacts"
# Make sure the artifacts directory exists
ARTIFACTS_DIR.mkdir(parents=True, exist_ok=True)
RUNTESTS_LOGFILE = ARTIFACTS_DIR.relative_to(REPO_ROOT) / "runtests-{}.log".format(
    datetime.datetime.now().strftime("%Y%m%d%H%M%S.%f")
)
COVERAGE_REPORT_DB = REPO_ROOT / ".coverage"
COVERAGE_REPORT_PROJECT = ARTIFACTS_DIR.relative_to(REPO_ROOT) / "coverage-project.xml"
COVERAGE_REPORT_TESTS = ARTIFACTS_DIR.relative_to(REPO_ROOT) / "coverage-tests.xml"
JUNIT_REPORT = ARTIFACTS_DIR.relative_to(REPO_ROOT) / "junit-report.xml"
IS_WINDOWS = sys.platform.startswith("win")
IS_DARWIN = sys.platform.lower().startswith("darwin")


def _get_session_python_version_info(session):
    try:
        version_info = session._runner._real_python_version_info
    except AttributeError:
        session_py_version = session.run_always(
            "python",
            "-c" 'import sys; sys.stdout.write("{}.{}.{}".format(*sys.version_info))',
            silent=True,
            log=False,
        )
        version_info = tuple(
            int(part) for part in session_py_version.split(".") if part.isdigit()
        )
        session._runner._real_python_version_info = version_info
    return version_info


def _get_pydir(session):
    version_info = _get_session_python_version_info(session)
    if version_info < (3, 6):
        session.error("Only Python >= 3.6 is supported")
    return "py{}.{}".format(*version_info)


@nox.session
def coverage(session):
    """
    Coverage analysis.
    """
    session.install(COVERAGE_VERSION_REQUIREMENT, silent=PIP_INSTALL_SILENT)
    # Always combine and generate the XML coverage report
    try:
        session.run("coverage", "combine")
    except CommandFailed:
        # Sometimes some of the coverage files are corrupt which would
        # trigger a CommandFailed exception
        pass
    try:
        # Generate report for salt code coverage
        session.run(
            "coverage",
            "xml",
            "-o",
            str(COVERAGE_REPORT_PROJECT),
            "--omit=tests/*",
            "--include=salt/*",
        )
    except CommandFailed:
        # Don't fail if there isn't any
        pass
    # Generate report for tests code coverage
    session.run(
        "coverage",
        "xml",
        "-o",
        str(COVERAGE_REPORT_TESTS),
        "--omit=salt/*",
        "--include=tests/*",
    )
    # Move the coverage DB to artifacts/coverage in order for it to be archived by CI
    if COVERAGE_REPORT_DB.exists():
        shutil.move(
            str(COVERAGE_REPORT_DB), str(ARTIFACTS_DIR / COVERAGE_REPORT_DB.name)
        )


def _pytest(session, pytest_args):
    """
    Run pytest
    """
    if SKIP_REQUIREMENTS_INSTALL is False:
        pre_requirements = [
            COVERAGE_VERSION_REQUIREMENT,
            "wheel",
            "cython",
        ]
        session.install(
            "--progress-bar=off",
            *pre_requirements,
            silent=PIP_INSTALL_SILENT,
        )

        env = {}
        if IS_WINDOWS:
            # We might need to update this if we bump to a more recent
            # version of salt. Do note that this version of salt is NOT
            # what is being tested. It only provides the required code
            # for pytest-salt-factories to run
            session.install(
                "--only-binary=:all:",
                "--no-binary=:none:",
                "--progress-bar=off",
                "pythonnet==2.5.2",
                "pymssql==2.2.1",
                "pycurl==7.43.0.5",
                silent=PIP_INSTALL_SILENT,
            )
            env["USE_STATIC_REQUIREMENTS"] = "0"
            requirements_file = REPO_ROOT / "requirements" / "tests.in"
        elif IS_DARWIN:
            # We might need to update this if we bump to a more recent
            # version of salt. Do note that this version of salt is NOT
            # what is being tested. It only provides the required code
            # for pytest-salt-factories to run
            session.install(
                "certifi==2021.5.30",
                "chardet==3.0.4",
                "idna==2.8",
                "immutables==0.15",
                "pycryptodomex==3.9.8",
                "requests==2.25.1",
                "urllib3==1.26.6",
                silent=PIP_INSTALL_SILENT,
            )
            # env["USE_STATIC_REQUIREMENTS"] = "0"
            # requirements_file = REPO_ROOT / "requirements" / "tests.in"
            requirements_file = (
                REPO_ROOT / "requirements" / _get_pydir(session) / "tests.txt"
            )
        else:
            requirements_file = (
                REPO_ROOT / "requirements" / _get_pydir(session) / "tests.txt"
            )

        install_command = [
            "--progress-bar=off",
            "-r",
            str(requirements_file.relative_to(REPO_ROOT)),
        ]
        session.install(
            *install_command,
            env=env,
            silent=PIP_INSTALL_SILENT,
        )

        if EXTRA_REQUIREMENTS_INSTALL:
            session.log(
                "Installing the following extra requirements because the "
                "EXTRA_REQUIREMENTS_INSTALL environment variable was set: "
                "EXTRA_REQUIREMENTS_INSTALL='%s'",
                EXTRA_REQUIREMENTS_INSTALL,
            )
            install_command = ["--progress-bar=off"]
            install_command += [
                req.strip() for req in EXTRA_REQUIREMENTS_INSTALL.split()
            ]
            session.install(*install_command, silent=PIP_INSTALL_SILENT)

        # Remove all salt related CLI scripts installed from pip
        # We need to use salt as a library for salt-factories, we don't
        # need the CLI scripts, and, in fact, they would get picked first
        # over the ones that the salt packages being tested install.
        for path in session.bin_paths:
            for pattern in ("*salt*", "spm"):
                for binary in pathlib.Path(path).glob(pattern):
                    if binary.name == "salt-factories":
                        continue
                    try:
                        session.warn("Removing %s binary from the virtualenv", binary)
                    except AttributeError:
                        session.log("Removing %s binary from the virtualenv", binary)
                    binary.unlink()

    session.run("coverage", "erase")

    python_path_env_var = os.environ.get("PYTHONPATH") or None
    if python_path_env_var is None:
        python_path_env_var = str(SITECUSTOMIZE_DIR)
    else:
        python_path_entries = python_path_env_var.split(os.pathsep)
        if SITECUSTOMIZE_DIR in python_path_entries:
            python_path_entries.remove(SITECUSTOMIZE_DIR)
        python_path_entries.insert(0, SITECUSTOMIZE_DIR)
        python_path_env_var = os.pathsep.join(python_path_entries)

    env = {
        # The updated python path so that sitecustomize is importable
        "PYTHONPATH": python_path_env_var,
        # The full path to the .coverage data file. Makes sure we always write
        # them to the same directory
        "COVERAGE_FILE": str(COVERAGE_REPORT_DB),
        # Instruct sub processes to also run under coverage
        "COVERAGE_PROCESS_START": str(COVERAGE_REPORT_DB.with_name(".coveragerc")),
    }

    session.run("coverage", "run", "-m", "pytest", *pytest_args, env=env)
    session.notify("coverage")


@nox.session
def tests(session):
    """
    Test install of salt
    """
    args = [
        "--rootdir",
        str(REPO_ROOT),
        "--log-cli-level=warning",
        f"--log-file={RUNTESTS_LOGFILE}",
        "--log-file-level=debug",
        "--show-capture=no",
        f"--junitxml={JUNIT_REPORT}",
        "--showlocals",
        "--strict-markers",
        "-ra",
        "-s",
    ]
    if session._runner.global_config.forcecolor:
        for arg in session.posargs:
            if arg.startswith("--color"):
                break
        else:
            args.append("--color=yes")
    _pytest(session, args + session.posargs)


@nox.session
def upgrade_tests(session):
    """
    Install previous version of salt and upgrade.
    """
    args = [
        "--rootdir",
        str(REPO_ROOT),
        "--log-cli-level=warning",
        f"--log-file={RUNTESTS_LOGFILE}",
        "--log-file-level=debug",
        "--show-capture=no",
        f"--junitxml={JUNIT_REPORT}",
        "--showlocals",
        "--strict-markers",
        "-ra",
        "-s",
        "--upgrade",
        "--no-uninstall",
    ]

    if not IS_WINDOWS:
        args.append("tests/upgrade/test_salt_upgrade.py::test_salt_upgrade")
    if session._runner.global_config.forcecolor:
        for arg in session.posargs:
            if arg.startswith("--color"):
                break
        else:
            args.append("--color=yes")
    _pytest(session, args + session.posargs)
