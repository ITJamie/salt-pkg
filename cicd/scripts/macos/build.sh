#!/bin/bash
################################################################################
#
# Title: Build Salt Package Script for macOS
# Authors: CR Oldham, Shane Lee
# Date: December 2015
#
# Description: This script builds a Tiamat package from a compressed tarball
#              artifact. The artifact will be decompressed and the run directory
#              will be moved to /opt/salt/bin. The package will then be created
#              based on the contents of /opt/salt. The version will be obtained
#              from the artifact itself.
#
#              This script should be run with sudo. If not, it will attempt to
#              elevate and run with sudo. In order for the environment variables
#              to be available in sudo you need to pass the `-E` option. For
#              example:
#
#              sudo -E ./build.sh
#
# Requirements:
#     - Xcode
#
# Usage:
#     This script accepts one argument:
#
#         -s | --sign
#             Sign the package
#
#     Example:
#         sudo -E ./build.sh
#
# Environment Setup:
#     This script requires certificates and environment variables be present on
#     the system. They are used to sign the package.
#
#     Import Certificates:
#         Import the Salt Developer Application and Installer Signing
#         certificates using the following commands:
#
#         security import "developerID_application.p12" -k ~/Library/Keychains/login.keychain
#         security import "developerID_installer.p12" -k ~/Library/Keychains/login.keychain
#
#     Define Environment Variables:
#         Define the environment variables using the following commands (replace
#         with the actual values):
#
#         export CICD_DEV_APP_CERT="Developer ID Application: Salt Stack, Inc. (AB123ABCD1)"
#         export CICD_DEV_INSTALL_CERT="Developer ID Installer: Salt Stack, Inc. (AB123ABCD1)"
#
#         Don't forget to run sudo with the `-E` option so that the environment
#         variables are passed to the sudo environment.
#
################################################################################
echo "========================================================================="
echo "Salt MacOS Build Tiamat Package Script"
echo "========================================================================="

# Make sure the script is launched with sudo
if [[ $(id -u) -ne 0 ]]; then
    echo ">>>>>> Re-launching as sudo <<<<<<"
    exec sudo -E /bin/bash -c "$(printf '%q ' "${BASH_SOURCE[0]}" "$@")"
fi

# Script Functions
log () {
    echo -n "- $1: "
}

success () {
    # 2 is green, 7 is white
    echo "$(tput setaf 2)Success$(tput setaf 7)"
}

failed () {
    # 1 is red, 7 is white
    echo "$(tput setaf 1)Failed$(tput setaf 7)"
}

# Setting Variables
log "Setting Variables"
SRC_DIR=$(git rev-parse --show-toplevel)
ARTIFACTS_DIR=$SRC_DIR/artifacts
CPU_ARCH=$(uname -m)
SALT_DIR="/opt/salt"
BIN_DIR="$SALT_DIR/bin"
TMP_DIR=$(mktemp -d)
PKG_DIR="$TMP_DIR/salt_pkg"
PKG_RESOURCES=$SRC_DIR/cicd/scripts/macos
CMD_OUTPUT=$(mktemp -t cmd.log)
SCRIPT_DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)
success

# SIGN_PACKAGE can be passed as an environment variable or a switch. Environment
# variables take precedence
log "Checking SIGN_PACKAGE"
SIGN_PACKAGE=0
while true; do
    if [ -z "$1" ]; then break; fi
    case "$1" in
        -s | --sign )
            SIGN_PACKAGE=1;
            shift;
            ;;
        * )
            shift;
            ;;
    esac
done
if [ -n "${SIGN_PACKAGE}" ]; then
    success
else
    echo "SIGN_PACKAGE not set"
    failed
    exit 1
fi

if [ "$SIGN_PACKAGE" = "1" ]; then
    # Validate CICD_DEV_APP_CERT
    log "Validating Application Certificate"
    if [ -n "${CICD_DEV_APP_CERT}" ]; then
        success
    else
        failed
        echo "CICD_DEV_APP_CERT is not defined. Can't sign binaries."
        exit 1
    fi

    # Validate CICD_DEV_INSTALL_CERT
    log "Validating Installer Certificate"
    if [ -n "${CICD_DEV_INSTALL_CERT}" ]; then
        success
    else
        failed
        echo "CICD_DEV_INSTALL_CERT is not defined. Can't sign the package."
        exit 1
    fi

    # Validate CICD_DEV_PASSWORD
    log "Validating Password"
    if [ -n "${CICD_DEV_PASSWORD}" ]; then
        success
    else
        failed
        echo "CICD_DEV_PASSWORD is not defined. Can't sign the package."
        exit 1
    fi

    # Validate CICD_DEV_KEYCHAIN
    log "Validating Keychain"
    if [ -n "${CICD_DEV_KEYCHAIN}" ]; then
        success
    else
        failed
        echo "CICD_DEV_KEYCHAIN is not defined. Can't sign the package."
        exit 1
    fi
fi

# Validate Artifacts Dir
log "Validating Artifacts Directory"
if [ -d "$ARTIFACTS_DIR" ]; then
    success
else
    failed
    echo "Could not find artifacts directory at $ARTIFACTS_DIR"
    exit 1
fi

log "Cleaning Salt Directory"
rm -rf "$SALT_DIR"
if [ -d "$SALT_DIR" ]; then
    failed
    echo "Failed to remove: $SALT_DIR"
    exit 1
fi
mkdir "$SALT_DIR"
chown "$USER":staff "$SALT_DIR"
if [ -d "$SALT_DIR" ]; then
    success
else
    failed
    echo "Failed to create: $SALT_DIR"
    exit 1
fi

log "Cleaning Staging Area"
rm -rdf "$PKG_DIR"
if [ -d "$PKG_DIR" ]; then
    failed
    echo "Failed to remove: $PKG_DIR"
    exit 1
fi
mkdir -p "$PKG_DIR"
if [ -d "$PKG_DIR" ]; then
    success
else
    failed
    echo "Failed to create: $PKG_DIR"
    exit 1
fi

# Find the zip file in the artifacts directory
log "Searching for artifact"
tarball=$(find "$ARTIFACTS_DIR" -name "*.tar.gz" -print -quit)
if [ -n "$tarball" ]; then
    success
else
    failed
    echo "Could not find *.tar.gz in: $ARTIFACTS_DIR"
    exit 1
fi

log "Getting version from artifact"
dir_name=$(basename "$(dirname "$tarball")")
IFS="-"
read -ra version <<< "$dir_name"
if [ -n "${version[0]}" ]; then
    success
else
    failed
    echo "Failed to extract version from artifact directory: $dir_name"
    exit 1
fi

log "Decompressing tarball artifact"
tar -xzvf "$tarball" -C "$TMP_DIR" &> /dev/null
if [ -f "$TMP_DIR/salt/run/run" ]; then
    success
else
    failed
    echo "Failed to decompress: $tarball"
    exit 1
fi

log "Moving run folder to bin"
mv "$TMP_DIR/salt/run/" "$BIN_DIR/" &> /dev/null
if [ -f "$BIN_DIR/run" ]; then
    success
else
    failed
    echo "Failed to move run directory to $BIN_DIR/bin"
    exit 1
fi

################################################################################
# Sign Binaries built by Salt
################################################################################
if [ "$SIGN_PACKAGE" = "1" ]; then
    # First we have to unlock the keychain so we aren't prompted for a password
    security unlock-keychain -p "$CICD_DEV_PASSWORD" "$CICD_DEV_KEYCHAIN"

    log "Signing run with entitlements"
    if ! codesign --timestamp \
                  --options=runtime \
                  --verbose \
                  --force \
                  --entitlements "$SCRIPT_DIR/entitlements.plist" \
                  --sign "$CICD_DEV_APP_CERT" \
                  "$BIN_DIR/run" > "$CMD_OUTPUT" 2>&1; then
        failed
        echo "Failed to sign run with entitlements"
        echo "output >>>>>>"
        cat "$CMD_OUTPUT" 1>&2
        echo "<<<<<< output"
        exit 1
    fi
    success

    log "Signing dynamic libraries (*dylib)"
    if ! find ${BIN_DIR} \
        -type f \
        -name "*dylib" \
        -follow \
        -exec codesign --timestamp \
                       --options=runtime \
                       --verbose \
                       --force \
                       --sign "$CICD_DEV_APP_CERT" "{}" \; > "$CMD_OUTPUT" 2>&1; then
        failed
        echo "Failed to sign dynamic libraries"
        echo "output >>>>>>"
        cat "$CMD_OUTPUT" 1>&2
        echo "<<<<<< output"
        exit 1
    fi
    success

    log "Signing shared libraries (*.so)"
    if ! find ${BIN_DIR} \
        -type f \
        -name "*.so" \
        -follow \
        -exec codesign --timestamp \
                       --options=runtime \
                       --verbose \
                       --force \
                       --sign "$CICD_DEV_APP_CERT" "{}" \; > "$CMD_OUTPUT" 2>&1; then
        failed
        echo "Failed to sign shared libraries"
        echo "output >>>>>>"
        cat "$CMD_OUTPUT" 1>&2
        echo "<<<<<< output"
        exit 1
    fi
    success
fi

################################################################################
# Stage Package Resources
################################################################################
log "Staging Helper Scripts"
find "$PKG_RESOURCES/scripts" \
    -type f \
    -name "salt*.sh" \
    -exec cp "{}" "$BIN_DIR" \; &> /dev/null
if [ -f "$BIN_DIR/salt.sh" ]; then
    success
else
    failed
    echo "Failed to copy helper scripts"
    exit 1
fi

log "Staging Tiamat Files"
mkdir -p "$PKG_DIR/opt/salt" &> /dev/null
cp -R "$BIN_DIR" "$PKG_DIR$BIN_DIR" &> /dev/null
if [ -f "$PKG_DIR$BIN_DIR/run" ]; then
    success
else
    failed
    echo "Failed to copy bin to staging"
    exit 1
fi

log "Staging Service Definitions"
mkdir -p "$PKG_DIR/Library/LaunchDaemons"
find "$PKG_RESOURCES/scripts" \
    -type f \
    -name "*.plist" \
    -exec cp "{}" "$PKG_DIR/Library/LaunchDaemons" \; &> /dev/null
if [ -f "$PKG_DIR/Library/LaunchDaemons/com.saltstack.salt.api.plist" ]; then
    success
else
    failed
    echo "Failed to copy plist files"
    exit 1
fi

mkdir -p "$PKG_DIR/etc/salt" &> /dev/null
if [ -f "$ARTIFACTS_DIR/minion" ]; then
    log "Staging Minion Config"
    mv "$ARTIFACTS_DIR/minion" "$PKG_DIR/etc/salt/minion.dist" &> /dev/null
    if [ -f "$PKG_DIR/etc/salt/minion.dist" ]; then
        success
    else
        failed
        echo "Failed to copy minion config"
        exit 1
    fi
fi
if [ -f "$ARTIFACTS_DIR/master" ]; then
    log "Staging Master Config"
    mv "$ARTIFACTS_DIR/master" "$PKG_DIR/etc/salt/master.dist" &> /dev/null
    if [ -f "$PKG_DIR/etc/salt/master.dist" ]; then
        success
    else
        failed
        echo "Failed to copy master config"
        exit 1
    fi
fi

################################################################################
# Add Title, Description, Version and CPU Arch to distribution.xml
################################################################################
log "Modifying distribution.xml"

TITLE="Salt $version(Tiamat)"
DESC="Salt $version built with Tiamat"

cd "$PKG_RESOURCES" || exit 1
cp distribution.xml.dist distribution.xml

SEDSTR="s/@TITLE@/$TITLE/g"
sed -E -i '' "$SEDSTR" distribution.xml
status=$?
if [ $status -ne 0 ]; then
    failed
    echo "Failed to set Title"
    exit 1
fi

SEDSTR="s/@DESC@/$DESC/g"
sed -E -i '' "$SEDSTR" distribution.xml
status=$?
if [ $status -ne 0 ]; then
    failed
    echo "Failed to set Desc"
    exit 1
fi

SEDSTR="s/@VERSION@/$version/g"
sed -E -i '' "$SEDSTR" distribution.xml
status=$?
if [ $status -ne 0 ]; then
    failed
    echo "Failed to set Version"
    exit 1
fi

SEDSTR="s/@CPU_ARCH@/$CPU_ARCH/g"
sed -i '' "$SEDSTR" distribution.xml
status=$?
if [ $status -ne 0 ]; then
    failed
    echo "Failed to set CPU Arch"
    exit 1
else
    success
fi

################################################################################
# Build and Sign the Package
################################################################################
log "Building the Source Package"

# Build the src package
pkgbuild --root="$PKG_DIR" \
         --scripts=pkg-scripts \
         --identifier=com.saltstack.salt \
         --version="$version" \
         --ownership=recommended \
         "salt-src-$version-Tiamat-$CPU_ARCH.pkg" &> /dev/null

status=$?
if [ $status -ne 0 ]; then
    failed
    echo "pkgbuild failed: $status"
    exit 1
fi

if [ -f "./salt-src-$version-Tiamat-$CPU_ARCH.pkg" ]; then
    success
else
    failed
    echo "Failed to build source package"
    exit 1
fi

if [ "$SIGN_PACKAGE" = "1" ]; then
    PACKAGE_NAME="salt-$version-Tiamat-$CPU_ARCH-signed.pkg"
    log "Building and Signing the Product Package with Timestamp"
    productbuild --resources=pkg-resources \
                 --distribution=distribution.xml  \
                 --package-path="salt-src-$version-Tiamat-$CPU_ARCH.pkg" \
                 --version="$version" \
                 --sign "$CICD_DEV_INSTALL_CERT" \
                 --timestamp \
                 "$PACKAGE_NAME" &> /dev/null
else
    log "Building the Product Package unsigned"
    PACKAGE_NAME="salt-$version-Tiamat-$CPU_ARCH-unsigned.pkg"
    productbuild --resources=pkg-resources \
                 --distribution=distribution.xml  \
                 --package-path="salt-src-$version-Tiamat-$CPU_ARCH.pkg" \
                 --version="$version" \
                 "$PACKAGE_NAME" &> /dev/null
fi

status=$?
if [ $status -ne 0 ]; then
    failed
    echo "productbuild failed: $status"
    exit 1
fi

if [ -f "$PACKAGE_NAME" ]; then
    success
else
    failed
    echo "Failed to build and sign product package"
    exit 1
fi

echo "========================================================================="
echo "Salt Package Build Script Complete"
echo "========================================================================="
