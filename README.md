# salt-pkg: Building Salt for Your Consumption

`salt-pkg` builds and packages salt, using tools like [Tiamat](https://gitlab.com/saltstack/pop/tiamat), for a variety of systems.

<!-- ABOUT THE PROJECT -->
## About The Project

This repo is used to generate tiamat builds of Salt. Tiamat uses pyinstaller to create
self contained binaries of Salt. These binaries include python, all of Salt's
default requirements and any additional requirements specified during the build.
This allows users to use Salt without python being installed on the box.

## Using Tiamat builds of salt

Tiamat-generated builds of `salt` are in **beta**, and are ready for testing.

### Installing debian-based packages with `apt`

If wanting to install on debian/Ubuntu-based systems using the `apt` package manager, follow the directions for the appropriate OS target.

Supported OS versions (`amd64`-only):

- Ubuntu 20.04 / focal
- Ubuntu 18.04 / bionic
- Debian 9 / stretch
- Debian 10 / buster

```bash
DISTRO_NAME=$(grep '^ID=' /etc/os-release | cut -d'=' -f2 | cut -d'"' -f2)
DISTRO_VERSION=$(grep 'VERSION_ID=' /etc/os-release | cut -d'=' -f2 | cut -d'"' -f2)

# Pull down key, configure repo
sudo curl -fsSL -o /usr/share/keyrings/salt-archive-keyring.gpg https://repo.saltproject.io/salt-dev/py3/${DISTRO_NAME}/${DISTRO_VERSION}/amd64/salt-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/salt-archive-keyring.gpg] https://repo.saltproject.io/salt-dev/py3/${DISTRO_NAME}/${DISTRO_VERSION}/amd64 stable main" | sudo tee /etc/apt/sources.list.d/salt-dev.list
```

```bash
apt-get update

# Install salt packages as normal
# Example for salt-minion
apt-get install --yes salt-minion
```

### Installing RHEL/CentOS-based packages with `yum`

If wanting ot install on RHEL/CentOS-based systems using the `yum` package manager, follow the directions for the appropriate OS target.

#### CentOS 7

```bash
sudo rpm --import https://repo.saltproject.io/salt-dev/py3/redhat/7/x86_64/SALTSTACK-GPG-KEY.pub
curl -fsSL https://repo.saltproject.io/salt-dev/py3/redhat/7/x86_64/dev.repo | sudo tee /etc/yum.repos.d/salt-dev.repo
```

```bash
yum clean expire-cache

# Install salt packages as normal
# Example for salt-minion
yum -y install salt-minion
```

#### CentOS 8

```bash
sudo rpm --import https://repo.saltproject.io/salt-dev/py3/redhat/8/x86_64/SALTSTACK-GPG-KEY.pub
curl -fsSL https://repo.saltproject.io/salt-dev/py3/redhat/8/x86_64/dev.repo | sudo tee /etc/yum.repos.d/salt-dev.repo
```

```bash
yum clean expire-cache

# Install salt packages as normal
# Example for salt-minion
yum -y install salt-minion
```

#### Amazon Linux 2

```bash
sudo rpm --import https://repo.saltproject.io/salt-dev/py3/amazon/2/x86_64/SALTSTACK-GPG-KEY.pub
curl -fsSL https://repo.saltproject.io/salt-dev/py3/amazon/2/x86_64/dev.repo | sudo tee /etc/yum.repos.d/salt-dev.repo
```

```bash
yum clean expire-cache

# Install salt packages as normal
# Example for salt-minion
yum -y install salt-minion
```

### How Tiamat-generated `salt` works

When installing from `https://repo.saltproject.io/salt-dev/*` repositories (following the steps above), the following is the result:

- **These are nightly builds, with a version output representing the current build installed.**
- `salt-master`, `salt-minion`, and other `salt` calls are really just bash-shell scripts that call out to `/opt/saltstack/salt/run/run <master|minion|call|etc.>` (with `run` being the Tiamat-generated, [PyInstaller](https://www.pyinstaller.org/) launcher of `salt`).

All of `salt` is contained within `/opt/saltstack/salt/`, with the primary calls being launched from `/opt/saltstack/salt/run/run`. `run` takes args to provide functionality similar to how `salt-master`, `salt-minion`, `salt-cloud`, etc. all worked.

If calling directly from the binary, it would work like `run <type> <args>`:

- `run master <args>` == `salt-master <args>`
- `run minion <args>` == `salt-minion <args>`


## Building Tiamat builds of salt

### Install Build Requirements

You can easily build a Salt binary using the contents from the `salt-pkg` repo. To build your own
binary, first clone the `salt-pkg` repo.

```
$ git clone https://gitlab.com/saltstack/open/salt-pkg.git
```

You will also need to install all of the dependencies.

```
pip install -r requirements.txt
```


### Run pkgr to build the binary

The salt-pkg repo uses the [pkgr](https://gitlab.com/saltstack/pop/pkgr) tool to
build the binaries. Currently this repo can build the following package types:
 - single binaries
 - onedir
 - rpm
 - deb

After the dependencies are installed you can run pkgr to build the binary:

```
pkgr --log-level=debug -c `<build type config file>` --git=https://github.com/saltstack/salt.git
```

The different build type config files:
- single binary: pkgs/single_bin/binary.conf
- onedir: pkgs/one_dir/onedir.conf
- rpm: pkgs/redhat/redhat.conf
- deb: pkgs/debian/debian.conf

For example if you want to build a rpm, you would run the command:

```
pkgr --log-level=debug -c pkgs/redhat/redhat.conf --git=https://github.com/saltstack/salt.git
```

The above command will build a rpm binary from the master branch of Salt by default. If you want to build from a different repo and ref you would run:

```
pkgr --log-level=debug -c <build type config file> --git=https://github.com/username/salt.git --ref=branch_name
```

### Including additional requirements in a build of Salt

If you want to add additional requirements to be included in your build you have a couple of options.
* **Edit salt-pkg requirements.txt file**:

  * You can add the name of the extra dependency in the `salt-pkg` repo in the build types requirement file before running pkgr.
    Here are all of the different build type's requirement files:
    - single binary: pkgs/single_bin/sources/requirements.txt
    - onedir: pkgs/one_dir/sources/requirements.txt
    - rpm: pkgs/redhat/sources/requirements.txt
    - deb: pkgs/debian/debian/requirements.txt

* **Edit the requirement files in the Salt repo**:

  * You can edit the requirements files in the Salt repo using the `--ref` and `--git` `pkgr` args, and then point to your repo and ref of the commit, branch, or tag that contains the change.


### Single Binary vs One Dir

#### Single Binary
* The single binary build uses PyInstaller's one-file build to create a single file executable.
* The single binary will be slower when initially starting up the Salt Minion, because
  it needs to extract all of the files to TEMP before running the binary.
* The single binary will not work when attempting to daemonize a salt process (`salt -d`). You
  will run into this issue: https://gitlab.com/saltstack/open/salt-pkg/-/issues/27. The single
  binary by design will delete the running TMP directory when it forks the process.
  You can workaround this issue by running salt in the foreground or using systemd to manage the process.

#### OneDir
* The onedir binary build uses PyInstaller's one-dir build to create a bundle containing
the Salt binary.


## Issues
Please check the issues in this repository for any known issues and open any new issues you find during testing.
